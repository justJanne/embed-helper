package main

import (
	"encoding/json"
	"fmt"
	"golang.org/x/net/html"
	"net/http"
	"net/url"
	"strings"
)

func returnJson(w http.ResponseWriter, data interface{}) error {
	marshalled, err := json.Marshal(data)
	if err != nil {
		return err
	}

	w.Header().Add("Content-Type", "application/json")
	if _, err := w.Write(marshalled); err != nil {
		return err
	}

	return nil
}

type OEmbed struct {
	Url          string `json:"url"`
	AuthorName   string `json:"author_name"`
	AuthorUrl    string `json:"author_url"`
	Html         string `json:"html"`
	Width        int    `json:"width"`
	Height       int    `json:"height"`
	Type         string `json:"type"`
	CacheAge     string `json:"cache_age"`
	ProviderName string `json:"provider_name"`
	ProviderUrl  string `json:"provider_url"`
	Version      string `json:"version"`
}

type InternalData struct {
	url *url.URL

	oembedUrl string

	twitterCard              string
	twitterSite              string
	twitterSiteId            string
	twitterCreator           string
	twitterCreatorId         string
	twitterDescription       string
	twitterTitle             string
	twitterImage             string
	twitterImageAlt          string
	twitterPlayer            string
	twitterPlayerWidth       string
	twitterPlayerHeight      string
	twitterPlayerStream      string
	twitterAppNameIphone     string
	twitterAppIdIphone       string
	twitterAppUrlIphone      string
	twitterAppNameIpad       string
	twitterAppIdIpad         string
	twitterAppUrlIpad        string
	twitterAppNameGoogleplay string
	twitterAppIdGoogleplay   string
	twitterAppUrlGoogleplay  string
	twitterLabel1            string
	twitterData1             string
	twitterLabel2            string
	twitterData2             string

	ogUrl         string
	ogTitle       string
	ogDescription string
	ogType        string
	ogLocale      string

	ogVideo          string
	ogVideoUrl       string
	ogVideoSecureUrl string
	ogVideoType      string
	ogVideoWidth     string
	ogVideoHeight    string

	ogImage          string
	ogImageUrl       string
	ogImageSecureUrl string
	ogImageType      string
	ogImageWidth     string
	ogImageHeight    string

	ogAudio          string
	ogAudioUrl       string
	ogAudioSecureUrl string
	ogAudioType      string

	ogSiteName string

	articleAuthor        string
	articlePublisher     string
	articlePublishedTime string

	soundcloudUser          string
	soundcloudPlayCount     string
	soundcloudDownloadCount string
	soundcloudCommentsCount string
	soundcloudLikeCount     string
	soundcloudSoundCount    string
	soundcloudFollowerCount string

	metaTitle       string
	metaDescription string
	metaAuthor      string
	metaThemeColor  string
	linkFavicon     string
	linkAuthor      string
	title           string

	urlProviderIconIco string
	urlProviderIconPng string

	urlProviderOgSiteName string

	urlProviderMetaThemeColor string
	urlProviderMetaTitle      string
	urlProviderLinkFavicon    string
	urlProviderTitle          string

	rawType string
}

type DataField struct {
	Title string `json:"title"`
	Value string `json:"value"`
	Short bool   `json:"short"`
}

type Data struct {
	FromUrl     string      `json:"from_url"`
	Color       string      `json:"color"`
	AuthorName  string      `json:"author_name"`
	AuthorLink  string      `json:"author_link"`
	AuthorIcon  string      `json:"author_icon"`
	Title       string      `json:"title"`
	TitleLink   string      `json:"title_link"`
	Text        string      `json:"text"`
	Fields      []DataField `json:"fields"`
	ImageUrl    string      `json:"image_url"`
	Type        string      `json:"type"`
	Player      string      `json:"player"`
	ServiceName string      `json:"service_name"`
	ServiceIcon string      `json:"service_icon"`
	Ts          int         `json:"ts"`
}

func coalesce(list []string) string {
	for _, str := range list {
		str = strings.TrimSpace(str)
		if str != "" {
			return str
		}
	}
	return ""
}

func coalesceFilter(condition func(string) bool, list []string) string {
	for _, str := range list {
		str = strings.TrimSpace(str)
		if str != "" && condition(str) {
			return str
		}
	}
	return ""
}

func isUrl(value string) bool {
	if strings.Contains(value, "://") {
		return true
	}
	if strings.HasPrefix(value, "/") {
		return true
	}
	return false
}

func isNotUrl(value string) bool {
	return !isUrl(value)
}

func resolve(base *url.URL, path string) string {
	if path == "" || base == nil {
		return ""
	}
	relative, err := url.Parse(path)
	if err != nil {
		return ""
	}
	absolute := base.ResolveReference(relative)
	return absolute.String()
}

func buildData(in InternalData, oEmbed OEmbed, providerFallbacks []func(*InternalData)) (out Data) {
	out = Data{}
	out.FromUrl = in.url.String()
	out.TitleLink = in.url.String()
	out.Title = coalesce([]string{in.twitterTitle, in.ogTitle, in.metaTitle, in.title})
	out.Text = coalesce([]string{in.twitterDescription, in.ogDescription, in.metaDescription})
	out.AuthorName = coalesceFilter(isNotUrl, []string{oEmbed.AuthorName, in.articleAuthor, in.articlePublisher, in.metaAuthor})
	out.AuthorLink = resolve(in.url, coalesceFilter(isUrl, []string{oEmbed.AuthorUrl, in.articleAuthor, in.articlePublisher, in.soundcloudUser, in.linkAuthor}))

	for _, fallback := range providerFallbacks {
		out.Color = coalesce([]string{in.metaThemeColor, in.urlProviderMetaThemeColor})
		out.ServiceName = coalesce([]string{oEmbed.ProviderName, in.ogSiteName, in.urlProviderOgSiteName, in.urlProviderMetaTitle, in.urlProviderTitle})
		out.ServiceIcon = resolve(in.url, coalesce([]string{in.linkFavicon, in.urlProviderLinkFavicon, in.urlProviderIconPng, in.urlProviderIconIco}))
		if out.ServiceName != "" && out.ServiceIcon != "" {
			break
		}
		fallback(&in)
	}

	var images []string
	images = append(images, in.twitterImage)
	images = append(images, in.ogImage)
	if in.rawType == "image" {
		images = append(images, in.url.String())
	}

	out.ImageUrl = resolve(in.url, coalesce(images))

	hasType := func(formats []string, types []string) bool {
		for _, format := range formats {
			for _, t := range types {
				if t == format {
					return true
				}
			}
		}
		return false
	}

	if in.rawType == "video" || in.rawType == "audio" {
		out.Player = in.url.String()
		out.Type = "player"
	} else if in.rawType == "image" {
		out.Type = "image"
	} else {
		if hasType([]string{oEmbed.Type, in.twitterCard, in.ogType}, []string{"article", "summary_large_image", "image"}) {
			out.Type = "image"
		} else if hasType([]string{oEmbed.Type, in.twitterCard, in.ogType}, []string{"player", "video", "video.other", "music", "music.song", "audio"}) {
			out.Type = "player"
		}

		if out.Type == "player" {
			out.Player = coalesce([]string{in.twitterPlayer, in.ogVideoSecureUrl, in.ogVideoUrl, in.ogAudioSecureUrl, in.ogAudioUrl})
		}
	}
	if out.Type == "" {
		out.Type = "article"
	}

	buildField := func(title string, value string) {
		const MaxShortFieldLength = 32
		value = strings.TrimSpace(value)
		if value != "" && value != "0" {
			out.Fields = append(out.Fields, DataField{
				Title: title,
				Value: value,
				Short: len(value) < MaxShortFieldLength && len(title) < MaxShortFieldLength,
			})
		}
	}

	buildField("Play Count", in.soundcloudPlayCount)
	buildField("Download Count", in.soundcloudDownloadCount)
	buildField("Comments Count", in.soundcloudCommentsCount)
	buildField("Like Count", in.soundcloudLikeCount)
	buildField("Sound Count", in.soundcloudSoundCount)
	buildField("Follower Count", in.soundcloudFollowerCount)

	return
}

func main() {
	client := &http.Client{}

	matchers := map[string]func(*InternalData, string, string){}
	matchers["meta/twitter:card"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterCard == "" {
            internalData.twitterCard = content
        }
	}
	matchers["meta/twitter:site"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterSite == "" {
            internalData.twitterSite = content
        }
	}
	matchers["meta/twitter:site:id"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterSiteId == "" {
            internalData.twitterSiteId = content
        }
	}
	matchers["meta/twitter:creator"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterCreator == "" {
            internalData.twitterCreator = content
        }
	}
	matchers["meta/twitter:creator:id"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterCreatorId == "" {
            internalData.twitterCreatorId = content
        }
	}
	matchers["meta/twitter:description"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterDescription == "" {
            internalData.twitterDescription = content
        }
	}
	matchers["meta/twitter:title"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterTitle == "" {
            internalData.twitterTitle = content
        }
	}
	matchers["meta/twitter:image"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterImage == "" {
            internalData.twitterImage = content
        }
	}
	matchers["meta/twitter:image:src"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterImage == "" {
            internalData.twitterImage = content
        }
	}
	matchers["meta/twitter:image:alt"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterImageAlt == "" {
            internalData.twitterImageAlt = content
        }
	}
	matchers["meta/twitter:player"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterPlayer == "" {
            internalData.twitterPlayer = content
        }
	}
	matchers["meta/twitter:player:width"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterPlayerWidth == "" {
            internalData.twitterPlayerWidth = content
        }
	}
	matchers["meta/twitter:player:height"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterPlayerHeight == "" {
            internalData.twitterPlayerHeight = content
        }
	}
	matchers["meta/twitter:player:stream"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterPlayerStream == "" {
            internalData.twitterPlayerStream = content
        }
	}
	matchers["meta/twitter:app:name:iphone"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterAppNameIphone == "" {
            internalData.twitterAppNameIphone = content
        }
	}
	matchers["meta/twitter:app:id:iphone"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterAppIdIphone == "" {
            internalData.twitterAppIdIphone = content
        }
	}
	matchers["meta/twitter:app:url:iphone"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterAppUrlIphone == "" {
            internalData.twitterAppUrlIphone = content
        }
	}
	matchers["meta/twitter:app:name:ipad"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterAppNameIpad == "" {
            internalData.twitterAppNameIpad = content
        }
	}
	matchers["meta/twitter:app:id:ipad"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterAppIdIpad == "" {
            internalData.twitterAppIdIpad = content
        }
	}
	matchers["meta/twitter:app:url:ipad"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterAppUrlIpad == "" {
            internalData.twitterAppUrlIpad = content
        }
	}
	matchers["meta/twitter:app:name:googleplay"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterAppNameGoogleplay == "" {
            internalData.twitterAppNameGoogleplay = content
        }
	}
	matchers["meta/twitter:app:id:googleplay"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterAppIdGoogleplay == "" {
            internalData.twitterAppIdGoogleplay = content
        }
	}
	matchers["meta/twitter:app:url:googleplay"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterAppUrlGoogleplay == "" {
            internalData.twitterAppUrlGoogleplay = content
        }
	}
	matchers["meta/twitter:label1"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterLabel1 == "" {
            internalData.twitterLabel1 = content
        }
	}
	matchers["meta/twitter:data1"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterData1 == "" {
            internalData.twitterData1 = content
        }
	}
	matchers["meta/twitter:label2"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterLabel2 == "" {
            internalData.twitterLabel2 = content
        }
	}
	matchers["meta/twitter:data2"] = func(internalData *InternalData, content string, extra string) {
		if internalData.twitterData2 == "" {
            internalData.twitterData2 = content
        }
	}

	matchers["meta/og:url"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogUrl == "" {
            internalData.ogUrl = content
        }
	}
	matchers["meta/og:title"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogTitle == "" {
            internalData.ogTitle = content
        }
	}
	matchers["meta/og:description"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogDescription == "" {
            internalData.ogDescription = content
        }
	}
	matchers["meta/og:type"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogType == "" {
            internalData.ogType = content
        }
	}
	matchers["meta/og:locale"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogLocale == "" {
            internalData.ogLocale = content
        }
	}

	matchers["meta/og:video"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogVideo == "" {
            internalData.ogVideo = content
        }
	}
	matchers["meta/og:video:url"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogVideoUrl == "" {
            internalData.ogVideoUrl = content
        }
	}
	matchers["meta/og:video:secure_url"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogVideoSecureUrl == "" {
            internalData.ogVideoSecureUrl = content
        }
	}
	matchers["meta/og:video:type"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogVideoType == "" {
            internalData.ogVideoType = content
        }
	}
	matchers["meta/og:video:width"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogVideoWidth == "" {
            internalData.ogVideoWidth = content
        }
	}
	matchers["meta/og:video:height"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogVideoHeight == "" {
            internalData.ogVideoHeight = content
        }
	}

	matchers["meta/og:image"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogImage == "" {
            internalData.ogImage = content
        }
	}
	matchers["meta/og:image:url"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogImageUrl == "" {
            internalData.ogImageUrl = content
        }
	}
	matchers["meta/og:image:secure_url"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogImageSecureUrl == "" {
            internalData.ogImageSecureUrl = content
        }
	}
	matchers["meta/og:image:type"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogImageType == "" {
            internalData.ogImageType = content
        }
	}
	matchers["meta/og:image:width"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogImageWidth == "" {
            internalData.ogImageWidth = content
        }
	}
	matchers["meta/og:image:height"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogImageHeight == "" {
            internalData.ogImageHeight = content
        }
	}

	matchers["meta/og:audio"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogAudio == "" {
            internalData.ogAudio = content
        }
	}
	matchers["meta/og:audio:url"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogAudioUrl == "" {
            internalData.ogAudioUrl = content
        }
	}
	matchers["meta/og:audio:secure_url"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogAudioSecureUrl == "" {
            internalData.ogAudioSecureUrl = content
        }
	}
	matchers["meta/og:audio:type"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogAudioType == "" {
            internalData.ogAudioType = content
        }
	}

	matchers["meta/og:site_name"] = func(internalData *InternalData, content string, extra string) {
		if internalData.ogSiteName == "" {
            internalData.ogSiteName = content
        }
	}

	matchers["meta/article:author"] = func(internalData *InternalData, content string, extra string) {
		if internalData.articleAuthor == "" {
            internalData.articleAuthor = content
        }
	}
	matchers["meta/article:publisher"] = func(internalData *InternalData, content string, extra string) {
		if internalData.articlePublisher == "" {
            internalData.articlePublisher = content
        }
	}
	matchers["meta/article:published_time"] = func(internalData *InternalData, content string, extra string) {
		if internalData.articlePublishedTime == "" {
            internalData.articlePublishedTime = content
        }
	}

	matchers["meta/soundcloud:user"] = func(internalData *InternalData, content string, extra string) {
		if internalData.soundcloudUser == "" {
            internalData.soundcloudUser = content
        }
	}
	matchers["meta/soundcloud:play_count"] = func(internalData *InternalData, content string, extra string) {
		if internalData.soundcloudPlayCount == "" {
            internalData.soundcloudPlayCount = content
        }
	}
	matchers["meta/soundcloud:download_count"] = func(internalData *InternalData, content string, extra string) {
		if internalData.soundcloudDownloadCount == "" {
            internalData.soundcloudDownloadCount = content
        }
	}
	matchers["meta/soundcloud:comments_count"] = func(internalData *InternalData, content string, extra string) {
		if internalData.soundcloudCommentsCount == "" {
            internalData.soundcloudCommentsCount = content
        }
	}
	matchers["meta/soundcloud:like_count"] = func(internalData *InternalData, content string, extra string) {
		if internalData.soundcloudLikeCount == "" {
            internalData.soundcloudLikeCount = content
        }
	}
	matchers["meta/soundcloud:sound_count"] = func(internalData *InternalData, content string, extra string) {
		if internalData.soundcloudSoundCount == "" {
            internalData.soundcloudSoundCount = content
        }
	}
	matchers["meta/soundcloud:follower_count"] = func(internalData *InternalData, content string, extra string) {
		if internalData.soundcloudFollowerCount == "" {
            internalData.soundcloudFollowerCount = content
        }
	}

	matchers["meta/title"] = func(internalData *InternalData, content string, extra string) {
		if internalData.metaTitle == "" {
            internalData.metaTitle = content
        }
	}
	matchers["meta/description"] = func(internalData *InternalData, content string, extra string) {
		if internalData.metaDescription == "" {
            internalData.metaDescription = content
        }
	}
	matchers["meta/author"] = func(internalData *InternalData, content string, extra string) {
		if internalData.metaAuthor == "" {
            internalData.metaAuthor = content
        }
	}
	matchers["meta/theme-color"] = func(internalData *InternalData, content string, extra string) {
		if internalData.metaThemeColor == "" {
            internalData.metaThemeColor = content
        }
	}

	matchers["link/icon"] = func(internalData *InternalData, content string, extra string) {
		if internalData.linkFavicon == "" {
            internalData.linkFavicon = content
        }
	}
	matchers["link/author"] = func(internalData *InternalData, content string, extra string) {
		if internalData.linkAuthor == "" {
            internalData.linkAuthor = content
        }
	}
	matchers["link/alternate"] = func(internalData *InternalData, content string, extra string) {
		if extra == "application/json+oembed" {
			if internalData.oembedUrl == "" {
            internalData.oembedUrl = content
        }
		}
	}
	matchers["title"] = func(internalData *InternalData, content string, extra string) {
		if internalData.title == "" {
            internalData.title = content
        }
	}

	providerMatchers := map[string]func(*InternalData, string, string){}

	providerMatchers["meta/og:site_name"] = func(internalData *InternalData, content string, extra string) {
		if internalData.urlProviderOgSiteName == "" {
            internalData.urlProviderOgSiteName = content
        }
	}
	providerMatchers["meta/title"] = func(internalData *InternalData, content string, extra string) {
		if internalData.urlProviderMetaTitle == "" {
            internalData.urlProviderMetaTitle = content
        }
	}
	providerMatchers["meta/theme-color"] = func(internalData *InternalData, content string, extra string) {
		if internalData.urlProviderMetaThemeColor == "" {
            internalData.urlProviderMetaThemeColor = content
        }
	}
	providerMatchers["link/icon"] = func(internalData *InternalData, content string, extra string) {
		if internalData.urlProviderLinkFavicon == "" {
            internalData.urlProviderLinkFavicon = content
        }
	}
	providerMatchers["title"] = func(internalData *InternalData, content string, extra string) {
		if internalData.urlProviderTitle == "" {
            internalData.urlProviderTitle = content
        }
	}

	loadData := func(requestedUrl string) (Data, error) {
		fmt.Printf("Searching for %s\n", requestedUrl)

		var data Data

		resp, err := client.Get(requestedUrl)
		if err != nil {
			return data, err
		}

		base, _ := url.ParseRequestURI(requestedUrl)

		var parseNode func(map[string]func(*InternalData, string, string), *InternalData, *html.Node)
		parseNode = func(matchers map[string]func(*InternalData, string, string), internalData *InternalData, n *html.Node) {
			if n.Type == html.ElementNode {
				attrs := map[string]string{}
				for _, attr := range n.Attr {
					attrs[attr.Key] = attr.Val
				}
				if n.Data == "meta" {
					name := coalesce([]string{attrs["name"], attrs["property"]})
					if name != "" {
						matcher := matchers["meta/"+name]
						if matcher != nil {
							matcher(internalData, attrs["content"], "")
						}
					}
				}
				if n.Data == "link" {
					names := strings.Split(attrs["rel"], " ")
					for _, name := range names {
						matcher := matchers["link/"+name]
						if matcher != nil {
							matcher(internalData, attrs["href"], attrs["type"])
						}
					}
				}
				if n.Data == "title" {
					c := n.FirstChild
					if c != nil && c.Type == html.TextNode {
						matcher := matchers["title"]
						if matcher != nil {
							matcher(internalData, c.Data, "")
						}
					}
				}
			}
			for c := n.FirstChild; c != nil; c = c.NextSibling {
				parseNode(matchers, internalData, c)
			}
		}

		internalData := InternalData{
			url: base,
		}

		contentType := strings.SplitN(resp.Header.Get("Content-Type"), ";", 2)[0]
		if contentType == "text/html" ||
			contentType == "application/xhtml+xml" ||
			contentType == "application/xhtml" ||
			contentType == "application/xml" {
			doc, _ := html.Parse(resp.Body)
			parseNode(matchers, &internalData, doc)
			err = resp.Body.Close()
			if err != nil {
				panic(err)
			}
		} else if strings.HasPrefix(contentType, "image/") {
			internalData.rawType = "image"
		} else if strings.HasPrefix(contentType, "video/") {
			internalData.rawType = "video"
		} else if strings.HasPrefix(contentType, "audio/") {
			internalData.rawType = "audio"
		}

		var oEmbedData OEmbed

		if internalData.oembedUrl != "" {
			fmt.Printf("Searching for %s\n", internalData.oembedUrl)
			resp, err := client.Get(internalData.oembedUrl)
			if err == nil {
				err = json.NewDecoder(resp.Body).Decode(&oEmbedData)
			} else {
				fmt.Println(err.Error())
			}
			err = resp.Body.Close()
			if err != nil {
				fmt.Println(err.Error())
			}
		}

		siteExists := func(siteUrl string) bool {
			resp, err := client.Get(siteUrl)
			if err == nil && resp.StatusCode == 200 {
				return true
			} else {
				return false
			}
		}

		providerFallback1 := func(internalData *InternalData) {
			providerUrl := resolve(internalData.url, "/")
			fmt.Printf("Searching for %s\n", providerUrl)
			providerResp, err := client.Get(providerUrl)
			if err == nil {
				contentType := strings.SplitN(providerResp.Header.Get("Content-Type"), ";", 2)[0]
				if contentType == "text/html" ||
					contentType == "application/xhtml+xml" ||
					contentType == "application/xhtml" ||
					contentType == "application/xml" {
					doc, _ := html.Parse(providerResp.Body)
					parseNode(providerMatchers, internalData, doc)
					err = providerResp.Body.Close()
					if err != nil {
						panic(err)
					}
				}
			}
		}

		providerFallback2 := func(internalData *InternalData) {
			providerFaviconUrl := resolve(internalData.url, "/favicon.png")
			fmt.Printf("Searching for %s\n", providerFaviconUrl)
			if siteExists(providerFaviconUrl) {
				internalData.urlProviderIconPng = providerFaviconUrl
			}
		}

		providerFallback3 := func(internalData *InternalData) {
			providerFaviconUrl := resolve(internalData.url, "/favicon.ico")
			fmt.Printf("Searching for %s\n", providerFaviconUrl)
			if siteExists(providerFaviconUrl) {
				internalData.urlProviderIconIco = providerFaviconUrl
			}
		}

		data = buildData(internalData, oEmbedData, []func(*InternalData){providerFallback1, providerFallback2, providerFallback3})
		return data, err
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		requestedUrl := strings.TrimSpace(r.URL.Query().Get("url"))
		if requestedUrl != "" {
			data, err := loadData(requestedUrl)
			if err != nil {
				fmt.Println(err.Error())
				err = returnJson(w, nil)
			} else {
				err = returnJson(w, data)
			}
			if err != nil {
				fmt.Println(err.Error())
				return
			}
		}
	})

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		panic(err)
	}
}
